# Downstream Pipelines Example

La configurazione delle downstream pipeline rappresenta una soluzione valida ed
efficace per gestire il flusso di lavoro all'interno di un repository GitLab.

La struttura di questo repository dimostra come le downstream pipeline di tipo
parent-child possono accelerare lo sviluppo delle pipeline attraverso
controllo, organizzazione e riutilizzo.

Per approfondire tecnicamente fare riferimento alla [documentazione
GitLab](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html)

## Organizzazione del repository

Per semplificare la dimostrazione, non vengono invocate pipeline da altri progetti.
Infatti, tutti i job sono raccolti nella cartella `jobs` del repository locale.
Questa decisione, tuttavia, non riduce la qualità dell'esercizio in quanto
l'unica differenza è la modalità di inclusione: `triggers:include:project` al
posto di `triggers:include:local`.

```
root
  |- README.md        # questo file
  |- jobs/            # contiene la definizione dei child job
  |- .gitlab-ci.yml   # l'orchestrazione dei job
```

## Panoramica sulla pipeline

Sempre all'insegna della semplificazione, tutti i job si limitano a stampare il
proprio nome e non effettuano un reale heavylifting.

La pipeline, che può più o meno essere ricondotta ad una realistica, è stato
pensata per evidenziati specifici casi d'uso delle downstream pipeline. Questa
è composta come segue:

| Stage             | Job                 | Trigger       | Need                                                      |
|-------------------|-------------------- |---------------|-----------------------------------------------------------|
| static            | lint:root           | always        | -                                                         |
| static            | lint:jobs           | always        | -                                                         |
| static            | sast                | always        | -                                                         |
| unit              | unit_test           | always        | lint:root,lint:src,sast                                   |
| build             | build               | always        | unit_test                                                 |
| preview           | deploy_preview      | merge_request | build                                                     |
| preview           | regression_preview  | merge_request | deploy_preview                                            |
| preview           | sca_preview         | merge_request | deploy_preview                                            |
| preview           | integration_preview | merge_request | deploy_preview                                            |
| preview           | compliance_preview  | merge_request | deploy_preview                                            |
| preview           | dast_preview        | merge_request | deploy_preview                                            |
| staging           | deploy_staging      | main          | build                                                     |
| quality_assurance | regression_qa       | main          | deploy_staging                                            |
| quality_assurance | sca_qa              | main          | deploy_staging                                            |
| quality_assurance | integration_qa      | main          | deploy_staging                                            |
| quality_assurance | compliance_qa       | main          | deploy_staging                                            |
| quality_assurance | dast_qa             | main          | deploy_staging                                            |
| pressure          | load_test           | main          | regression_qa,sca_qa,integration,qa,compliance_qa,dast_qa |
| production        | deploy_production   | manual        | load_test                                                 |

## Casi d'uso

Questo repository copre diversi casi d'uso. Navigando tra lo storico di
pipeline e di job è possibile vedere lo storico delle esecuzioni, tuttavia si
consiglia di visionare le seguenti pipeline:

- [Branch: main](https://gitlab.com/keypartner-examples/downstream-pipelines-example/-/pipelines/954580897)
- [Merge Request](https://gitlab.com/keypartner-examples/downstream-pipelines-example/-/pipelines)

#### 2 o più workload (job) dentro un child job

`jobs/lint.yml` definisce tre differenti workload utilizzando anche la
funzionalità di templating con la keyword `extends`.

![2_more_workload.png](docs/images/2_more_workload.png)

#### 2 o più child job dentro uno stage

Sia lo `static` che lo stage `quality assurance` eseguono più di child job.

![2_more_childjobs.png](docs/images/2_more_childjobs.png)

#### Inclusione multiple dello stesso child job

Nello stage `static` è importanto due volte il child job di linting. I due job
`lint:root` e `lint:jobs` sono eseguiti cono configurazioni differenti grazie
all'uso di variabili.

Il job di DAST è importanto due vole in stage differenti:

- `quality_assurance` con `dast_preview`;
- `dynamic` con `dast_staging`.

#### `only` nei trigger job

Con la keyword `only`, il job `deploy_preview` è eseguito solo su MR. Allo
stesso modo è possibile configurare un trigger job con innesco manuale.
Utilizzando lo stesso approccio gli stage a partire da `staging` sono eseguiti
solo su branch main.

![manual_trigger.png](docs/images/manual_trigger.png)

#### Workflow a livello globale (pipeline)

Con la keyword `workflow`, solo i cambiamenti alla pipeline o alla cartella
jobs innescano la pipeline.

```yaml
workflow:
  rules:
    - changes:
        - .gitlab-ci.yml
        - jobs/**/*
```

#### Workflow a livello locale (child job)

Con la keyword `workflow`, tutti i child job sono eseguiti solo se il
messaggio di commit non inizia con "chore".

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^chore/
      when: never
    - when: always
```
